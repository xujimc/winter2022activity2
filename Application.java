public class Application{
	public static void main(String[] args){
		Human son = new Human();
		Human friend = new Human();
		son.gender="attack helicopter";
		son.name="mike oxlong";
		son.age=40;
		friend.gender="fluid";
		friend.name="mike litoris";
		friend.age=30;
		System.out.println(son.name+" "+son.gender+" "+son.age);
		System.out.println(friend.name+" "+friend.gender+" "+friend.age);
		son.sayHi();
		friend.sayHi();
		Human[] circus = new Human[3];
		circus[0]=son;
		circus[1]=friend;
		System.out.println(circus[0].name);
		circus[2]=new Human();
		System.out.println(circus[2].name+" "+circus[2].gender+" "+circus[2].age);
	}
}