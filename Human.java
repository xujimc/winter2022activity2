public class Human{
	public String gender;
	public String name;
	public int age;
	public void sayHi(){
		System.out.println("Hello, I am "+this.name);
	}
	public void sayAge(){
		System.out.println("I am "+this.age+"years old");
	}
}